angular.module('finance', [])
.factory('currencyConverter', function() {
  var currencies = ['USD', 'EUR', 'GBP'];
  var usdToForeignRates = {
    USD: 1,
    EUR: 0.90,
    GBP: 0.70
  };
  var convert = function (amount, inCurr, outCurr) {
    return amount * usdToForeignRates[outCurr] / usdToForeignRates[inCurr];
  };

  return {
    currencies: currencies,
    convert: convert
  };
});

