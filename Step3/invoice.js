angular.module('invoiceApp', [])
.controller('InvoiceController', function() {
  this.qty = 1;
  this.cost = 2;
  this.inCurr = 'EUR';
  this.currencies = ['USD', 'EUR', 'GBP'];
  this.total = function total() {
    return this.qty * this.cost;
  };
});

