angular.module('finance', [])
.factory('currencyConverter', ['$http', function($http) {
  var URL_PATTERN =
        'http://query.yahooapis.com/v1/public/yql?q=select * from '+
        'yahoo.finance.xchange where pair in ("PAIRS")&format=json&'+
        'env=store://datatables.org/alltableswithkeys';

  var currencies = ['USD', 'EUR', 'GBP'];
  var usdToForeignRates = {};
  
  var convert = function (amount, inCurr, outCurr) {
    return amount * usdToForeignRates[outCurr] / usdToForeignRates[inCurr];
  };

  var refresh = function() {
    var url = URL_PATTERN.
               replace('PAIRS', 'USD' + currencies.join('","USD'));
    return $http.get(url).then(function(response) {
      var newUsdToForeignRates = {};
      var rates = response.data.query.results.rate;
      angular.forEach(rates, function(rate) {
        var currency = rate.id.substring(3,6);
        newUsdToForeignRates[currency] = window.parseFloat(rate.Rate);
      });
      usdToForeignRates = newUsdToForeignRates;
    });
  };

  refresh();

  return {
    currencies: currencies,
    convert: convert,
    refresh: refresh
  };
}]);

